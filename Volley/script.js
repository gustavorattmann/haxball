/* VARIABLES */

var AdminColor = "0xDA70D6";
var TeamRedColor = "0xFF0000";
var TeamBlueColor = "0x0000FF";
var SpecColor = 0xDEE7FA;
var RulesColor = "0x808080";

/* ROOM */

const roomName = "🏆 ✦ Portugal tem título no vôlei? ✦ 🏆";
const botName = "Áʀʙɪᴛʀᴏ ʙᴏᴛ 33";
const maxPlayers = 20;
const roomPublic = false;
const geo = [{"code": "BR", "lat": -23.5, "lon": -46.6}, {"code": "PT", "lat": 39.3, "lon": -8.2}];

const room = HBInit({ roomName: roomName, maxPlayers: maxPlayers, public: roomPublic, playerName: botName, geo: geo[0] });

const scoreLimit = 10;
const timeLimit = 5;
room.setScoreLimit(scoreLimit);
room.setTimeLimit(timeLimit);
room.setTeamsLock(true);

var volleyMap = '{"name":"volleyball from HaxMaps","width":420,"height":200,"spawnDistance":299,"bg":{"type":"hockey","width":0,"height":0,"kickOffRadius":0,"cornerRadius":0},"vertexes":[{"x":-5,"y":20,"trait":"net"},{"x":5,"y":20,"trait":"net"},{"x":-3,"y":120,"trait":"net"},{"x":100,"y":200,"trait":"kickOffBarrier"},{"x":100,"y":-200,"trait":"kickOffBarrier"},{"x":-100,"y":200,"trait":"kickOffBarrier"},{"x":-100,"y":-200,"trait":"kickOffBarrier"},{"x":-385,"y":100,"trait":"line"},{"x":-400,"y":125,"trait":"line"},{"x":400,"y":125,"trait":"line"},{"x":385,"y":100,"trait":"line"},{"x":-70,"y":-50,"trait":"line"},{"x":70,"y":-50,"trait":"line"},{"x":3,"y":120,"trait":"net"},{"x":-385,"y":103,"trait":"line","color":"CFB795"},{"x":385,"y":103,"trait":"line","color":"CFB795"},{"x":-387,"y":106,"trait":"line","color":"CFB795"},{"x":387,"y":106,"trait":"line","color":"CFB795"},{"x":-388,"y":109,"trait":"line","color":"CFB795"},{"x":388,"y":109,"trait":"line","color":"CFB795"},{"x":-391,"y":112,"trait":"line","color":"CFB795"},{"x":391,"y":112,"trait":"line","color":"CFB795"},{"x":-394,"y":115,"trait":"line","color":"CFB795"},{"x":394,"y":115,"trait":"line","color":"CFB795"},{"x":-396,"y":118,"trait":"line","color":"CFB795"},{"x":396,"y":118,"trait":"line","color":"CFB795"},{"x":-396,"y":121,"trait":"line","color":"CFB795"},{"x":396,"y":121,"trait":"line","color":"CFB795"},{"x":-396,"y":122,"trait":"line","color":"CFB795"},{"x":396,"y":122,"trait":"line","color":"CFB795"},{"x":3,"y":20,"trait":"net"},{"x":0,"y":23,"trait":"net","color":"FFFFFF"},{"trait":"line","x":100,"y":101},{"trait":"line","x":106,"y":124},{"trait":"line","x":-100,"y":101},{"trait":"line","x":-106,"y":124},{"x":-3,"y":20,"trait":"net"}],"segments":[{"v0":0,"v1":1,"trait":"net"},{"v0":3,"v1":4,"trait":"kickOffBarrier","cGroup":["redKO"]},{"v0":5,"v1":6,"trait":"kickOffBarrier","cGroup":["blueKO"]},{"v0":0,"v1":11,"trait":"kickOffBarrier","cGroup":["redKO"]},{"v0":1,"v1":12,"trait":"kickOffBarrier","cGroup":["blueKO"]},{"v0":8,"v1":9,"trait":"line"},{"v0":10,"v1":7,"trait":"line"},{"v0":15,"v1":14,"trait":"line","y":103,"color":"CFB795"},{"v0":17,"v1":16,"trait":"line","y":106,"color":"CFB795"},{"v0":19,"v1":18,"trait":"line","y":109,"color":"CFB795"},{"v0":21,"v1":20,"trait":"line","y":112,"color":"CFB795"},{"v0":23,"v1":22,"trait":"line","y":115,"color":"CFB795"},{"v0":25,"v1":24,"trait":"line","y":118,"color":"CFB795"},{"v0":27,"v1":26,"trait":"line","y":121,"color":"CFB795"},{"v0":29,"v1":28,"trait":"line","y":122,"color":"CFB795"},{"vis":true,"color":"FFFFFF","trait":"line","v0":8,"v1":7},{"vis":true,"color":"FFFFFF","trait":"line","v0":10,"v1":9},{"vis":true,"color":"FFFFFF","trait":"line","v0":32,"v1":33},{"vis":true,"color":"FFFFFF","trait":"line","v0":34,"v1":35}],"goals":[{"p0":[0,114],"p1":[420,114],"team":"blue"},{"p0":[0,114],"p1":[-420,114],"team":"red"}],"discs":[{"pos":[0,25],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,35],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,45],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,55],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,65],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,75],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,85],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,95],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,105],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,115],"trait":"net","radius":4,"color":"6D6C6B"},{"pos":[0,-100000],"trait":"ballPush"}],"planes":[{"normal":[0,-1],"dist":-210,"cGroup":["ball"],"bCoef":0},{"normal":[0,1],"dist":-50,"cMask":["red"],"bCoef":-1},{"normal":[0,1],"dist":-50,"cMask":["blue"],"bCoef":-1},{"normal":[0,-1],"dist":-125,"cMask":["red","blue","ball"],"bCoef":0},{"normal":[1,0],"dist":0,"cMask":["blue"],"bCoef":0.1},{"normal":[-1,0],"dist":0,"cMask":["red"],"bCoef":0.1},{"normal":[1,0],"dist":-420,"cMask":["ball","red","blue"],"bCoef":0.6},{"normal":[-1,0],"dist":-420,"cMask":["ball","red","blue"],"bCoef":0.6},{"normal":[0,1],"dist":-199790,"cGroup":["ball"],"bCoef":0},{"normal":[-1,0],"dist":-100000,"cGroup":["ball"],"bCoef":0},{"normal":[1,0],"dist":-100000,"cGroup":["ball"],"bCoef":0}],"playerPhysics":{"bCoef":0.5,"invMass":0.5,"damping":0.85,"acceleration":0.5,"kickingAcceleration":0.07,"kickingDamping":0.96,"kickStrength":20},"ballPhysics":{"radius":10,"bCoef":1,"invMass":0.9,"damping":0.98,"color":"FFFFFF","cMask":["wall"],"cGroup":["ball"]},"traits":{"net":{"vis":true,"bCoef":0,"invMass":0,"cMask":["ball"]},"line":{"vis":true,"cMask":[""],"color":"FFFFFF"},"redline":{"vis":true,"cMask":[""],"color":"FF4A4A"},"blueline":{"vis":true,"cMask":[""],"color":"6782FE"},"kickOffBarrier":{"vis":false,"bCoef":0.1,"cGroup":["redKO","blueKO"],"cMask":["red","blue"]},"ballPush":{"radius":100000,"invMass":60,"cMask":["ball"],"bCoef":0,"color":"FFFFFFFF"}}}';
room.setCustomStadium(volleyMap);

room.onStadiumChange = function (newStadiumName, byPlayer) {
  if (newStadiumName != "volleyball from HaxMaps") {
    room.setCustomStadium(volleyMap);
  }
}

var adminPassword = 100 + getRandomInt(900);
console.log("adminPassword: " + adminPassword);

/* OPTIONS */

var drawTimeLimit = 10;

/* PLAYERS */

const Team = { SPECTATORS: 0, RED: 1, BLUE: 2 };
var players;
var teamR;
var teamB;
var teamS;

/* GAME */

var lastPlayersTouched = [];
var point = [{"x": 0, "y": 0}, {"x": 0, "y": 0}];
var ballSpeed;
var goldenGoal = false;
var countPause;
var quantityPause;
var teamPause;
var playerSolicited = [];
var pauseNext;
var pauseAdmin;
var paused = false;

/* AUXILIARY */

var checkTimeVariable = false;

/* FUNCTIONS */

/* AUXILIARY FUNCTIONS */

function getRandomInt(max) { // return random number from 0 to max-1
  return Math.floor(Math.random() * Math.floor(max));
}

function arrayMin(arr) {
  var len = arr.length;
  var min = Infinity;
  
  while (len--) {
    if (arr[len] < min) {
      min = arr[len];
    }
  }
  
  return min;
}

function getTime(scores) {
  return "[" + Math.floor(Math.floor(scores.time/60)/10).toString() + Math.floor(Math.floor(scores.time/60)%10).toString() + ":" + Math.floor(Math.floor(scores.time - (Math.floor(scores.time/60) * 60))/10).toString() + Math.floor(Math.floor(scores.time - (Math.floor(scores.time/60) * 60))%10).toString() + "]"
}

function pointDistance(p1, p2) {
  var d1 = p1.x - p2.x;
  var d2 = p1.y - p2.y;
  return Math.sqrt(d1 * d1 + d2 * d2);
}

function regras() {
  room.sendAnnouncement("[Regras]:", undefined, undefined, "bold");
  room.sendAnnouncement("1 - Permitido apenas 3 toques por equipe em cada posse e 4 se houver bloqueio.", undefined, RulesColor, "bold");
  room.sendAnnouncement("2 - Não é permitido 2 toques seguidos por jogadores, tocou uma vez, deixa a próxima pro companheiro.", undefined, RulesColor, "bold");
  room.sendAnnouncement("3 - Alternar sacadores em cada posse.", undefined, RulesColor, "bold");
  room.sendAnnouncement("4 - Permitido apenas 3 pausas durante o jogo.", undefined, RulesColor, "bold");
}

function pausaTecnica() {
  if (!paused) {
    paused = true;
    room.pauseGame(true);
    room.sendAnnouncement('Pausa técnica solicitada pelo time ' + (teamPause == 1 ? 'vermelho' : 'azul') + '!', undefined, (teamPause == 1 ? TeamRedColor: TeamBlueColor), "bold");
    setTimeout(function() {
      room.pauseGame(false);
    }, 60000);
    
    quantityPause = 1;
    playerSolicited = [];
    teamPause = null;
    pauseNext = false;
    countPause++;
  }
}

function despausar() {
  if (paused) {
    paused = false;
    room.pauseGame(false);
    room.sendAnnouncement('Jogo despausado pelo time ' + (teamPause == 1 ? 'vermelho' : 'azul') + '!', undefined, (teamPause == 1 ? TeamRedColor: TeamBlueColor), "bold");
    
    quantityPause = 1;
    playerSolicited = [];
    teamPause = null;
    pauseNext = false;
  }
}

/* GAME FUNCTIONS */

function checkTime() {
  const scores = room.getScores();
  
  if (Math.abs(scores.time - scores.timeLimit) <= 0.01 && scores.timeLimit != 0) {
    if (scores.red != scores.blue) {
      if (checkTimeVariable == false) {
        checkTimeVariable = true;
        setTimeout(() => { checkTimeVariable = false; }, 3000);
        scores.red > scores.blue ? endGame(Team.RED) : endGame(Team.BLUE);
        setTimeout(() => { room.stopGame(); }, 2000);
      }
      
      return;
    }
    
    goldenGoal = true;
    room.sendChat("Vence o primeiro ponto");
  }
  
  if (Math.abs(drawTimeLimit * 60 - scores.time - 60) <= 0.01 && players.length > 2) {
    if (checkTimeVariable == false) {
      checkTimeVariable = true;
      setTimeout(() => { checkTimeVariable = false; }, 10);
      room.sendChat("Restam 60 segundos para acabar");
    }
  }
  
  if (Math.abs(scores.time - drawTimeLimit * 60) <= 0.01 && players.length > 2) {
    if (checkTimeVariable == false) {
      checkTimeVariable = true;
      setTimeout(() => { checkTimeVariable = false; }, 10);
      endGame(Team.SPECTATORS);
      room.stopGame();
      goldenGoal = false;
    }
  }
}

function endGame(winner) { // no stopGame() function in it
  const scores = room.getScores();
  
  if (winner == Team.RED) {
    room.sendAnnouncement("Time vermelho ganhou: " + scores.red + "-" + scores.blue + "!", undefined, TeamRedColor, "bold");
  } else if (winner == Team.BLUE) {
    room.sendAnnouncement("Time azul ganhou: " + scores.blue + "-" + scores.red + "!", undefined, TeamBlueColor, "bold");
  } else {
    room.sendChat("zzz Tempo limite esgotado! zzz");
  }
  
  countPause = null;
}

/* PLAYER FUNCTIONS */

function updateTeams() {
  players = room.getPlayerList().filter((player) => player.id != 0);
  teamR = players.filter(p => p.team === Team.RED);
  teamB = players.filter(p => p.team === Team.BLUE);
  teamS = players.filter(p => p.team === Team.SPECTATORS);
}

function updateAdmins() {
  if (players.length == 0 || players.find((player) => player.admin) != null) {
    return;
  }

  var copie = [];
  players.forEach(function(element) { copie.push(element.id); });
  room.setPlayerAdmin(arrayMin(copie), true); // Give admin to the player who's played the longest on the room
}

/* STATS FUNCTIONS */

function getStats() {
  const ballPosition = room.getBallPosition();
  point[1] = point[0];
  point[0] = ballPosition;
  ballSpeed = (pointDistance(point[0], point[1]) * 60 * 60 * 60)/15000;
}

/* EVENTS */

/* PLAYER MOVEMENT */

room.onPlayerJoin = function(player) {
  room.sendChat("[PM] Seja bem vindo " + player.name + "!");
  regras();
  room.sendChat("[PV] Para ver os comandos disponíveis, digite '!comandos'.");
  updateTeams();
  updateAdmins();
  
  console.log(players);
  console.log(player.conn);
  console.log(player.auth);
}

room.onPlayerTeamChange = function(changedPlayer, byPlayer) {
  if (changedPlayer.id == 0) {
    room.setPlayerTeam(0, Team.SPECTATORS);
    return;
  }
  
  updateTeams();
}

room.onPlayerLeave = function(player) {
  updateTeams();
  updateAdmins();
}

room.onPlayerKicked = function(kickedPlayer, reason, ban, byPlayer) {
}

/* PLAYER ACTIVITY */

room.onPlayerChat = function(player, message) {
  command = message.split(" ");
  
  if (["!comandos"].includes(command[0].toLowerCase())) {
		room.sendAnnouncement("[PV] Comandos de jogador: !regras, @pv #<id> <mensagem>, @time <mensagem>, .p, .unpause", player.id, undefined, undefined);
    player.admin ? room.sendAnnouncement("[PV] Comandos de admin: !setadmin, !removeadmin !mute <duration = 3> #<id>, !unmute all/#<id>, !clearbans <number = all>, !slow <duration>, !endslow", player.id, AdminColor, "bold") : null;
    player.admin ? room.sendAnnouncement("!senha <senha>, !limparsenha, !kick #<id> <motivo>, !pause, !unpause", player.id, AdminColor, "bold") : null;
    
    return false;
  } else if (["!regras"].includes(command[0].toLowerCase())) {
    regras();
	} else if (["@pv"].includes(command[0].toLowerCase())) {
    if (command[1].length > 0 && !Number.isNaN(Number.parseInt(command[1][1]))) {
      if (command[2].length > 0) {
        room.sendAnnouncement('[PV] ' + player.name + ': ' + message.substring(7).trim(), Number.parseInt(command[1][1]));
      }
    }
    
    return false;
  } else if (["@time"].includes(command[0].toLowerCase())) {
    if (command[1].length > 0) {
      if (player.team == 1) {
  			var players = room.getPlayerList().filter((player) => player.team == 1);
  			players.forEach(function(teamPlayer) {
  				room.sendAnnouncement("[Time] " + player.name + ": " + message.substring(6).trim(), teamPlayer.id, TeamRedColor, "normal", 1);
  			});
  		} else if (player.team == 2) {
  			var players = room.getPlayerList().filter((player) => player.team == 2);
  			players.forEach(function(teamPlayer) {
  				room.sendAnnouncement("[Time] " + player.name + ": " + message.substring(6).trim(), teamPlayer.id, TeamBlueColor, "normal", 1);
  			});
  		} else if (player.team == 0) {
  			var players = room.getPlayerList().filter((player) => player.team == 0);
  			players.forEach(function(teamPlayer) {
  				room.sendAnnouncement("[Spec] " + player.name + ": " + message.substring(6).trim(), teamPlayer.id, SpecColor, "normal", 1);
  			});
  		}
    }
    
    return false;
  } else if ([".p"].includes(command[0].toLowerCase())) {
    if (!pauseAdmin) {
      if ((room.getScores().blue < 9 && room.getScores().red < 9) && (room.getScores().time < 291)) {
        if (playerSolicited[player.id] == null || playerSolicited[player.id] != player.name) {
          playerSolicited[player.id] = player.name;
          
          if (player.team == 1 || player.team == 2) {
            if (countPause == null) {
              countPause = 1;
            }
            
            if (quantityPause == null) {
              quantityPause = 1;
            }
            
            if (teamPause == null) {
              teamPause = player.team;
            }
            
            var jogadores = room.getPlayerList().filter((jogador) => jogador.team == teamPause);
            
            if (countPause < 4) {
              if (jogadores.length > 2) {
                if (quantityPause < 4) {
                  if (quantityPause == 1) {
                    room.sendAnnouncement(player.name + ' pediu pausa técnica, faltam mais ' + quantityPause + ' votos!', undefined, undefined, "bold");
                    quantityPause++;
                  } else if (quantityPause == 2) {
                    room.sendAnnouncement(player.name + ' deu mais um voto para pausa técnica, falta mais ' + quantityPause + ' voto!', undefined, undefined, "bold");
                    quantityPause++;
                  } else if (quantityPause == 3) {
                    jogadores.forEach(function(teamPlayer) {
                      room.sendAnnouncement('Pausa técnica solicitada pelo seu time!', teamPlayer.id, (teamPause == 1 ? TeamRedColor: TeamBlueColor), "bold");
                    });
                    pauseNext = true;
                  }
                } else {
                  room.sendAnnouncement('Pausa técnica já solicitada, aguarde o fim da rodada!', undefined, undefined, "bold");
                }
              } else if (jogadores.length > 1) {
                if (quantityPause < 3) {
                  if (quantityPause == 1) {
                    room.sendAnnouncement(player.name + ' pediu pausa técnica, falta mais ' + quantityPause + ' voto!', undefined, undefined, "bold");
                    quantityPause++;
                  } else if (quantityPause == 2) {
                    jogadores.forEach(function(teamPlayer) {
                      room.sendAnnouncement('Pausa técnica solicitada pelo seu time!', teamPlayer.id, (teamPause == 1 ? TeamRedColor: TeamBlueColor), "bold");
                    });
                    pauseNext = true;
                  }
                } else {
                  room.sendAnnouncement('Pausa técnica já solicitada, aguarde o fim da rodada!', undefined, undefined, "bold");
                }
              } else {
                if (quantityPause < 2) {
                  if (quantityPause == 1) {
                    room.sendAnnouncement('Pausa técnica solicitada, aguarde o fim da rodada!', undefined, undefined, "bold");
                    pauseNext = true;
                  }
                }
              }
            } else {
              if (jogadores.length > 1) {
                room.sendAnnouncement('Seu time já usou as 3 pausas técnicas!', player.id, (teamPause == 1 ? TeamRedColor: TeamBlueColor), "bold");
              } else {
                room.sendAnnouncement('Você já usou as 3 pausas técnicas!', player.id, undefined, "bold");
              }
            }
          } else {
            room.sendAnnouncement('Pausa técnica disponível apenas para quem está jogando!', player.id, undefined, "bold");
          }
        } else {
          room.sendAnnouncement('Você já votou!', player.id, undefined, "bold");
        }
      } else {
        room.sendAnnouncement('Jogo quase acabando... Deixou pra pedir pause agora?', player.id, undefined, "bold");
      }
    }
    
    return false;
  } else if ([".unpause"].includes(command[0].toLowerCase())) {
    if (!pauseAdmin) {
      despausar();
    }
    
    return false;
  } else if (["!setadmin"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      if (command[1].length > 0 && !Number.isNaN(Number.parseInt(command[1][1]))) {
        room.setPlayerAdmin(Number.parseInt(command[1][1]), true);
        adminPassword = 100 + getRandomInt(900);
        room.sendAnnouncement('Agora você é um administrador', Number.parseInt(command[1][1]), AdminColor, "bold");
      }
    } else {
      room.sendAnnouncement("Apenas um administrador pode adicionar algum jogador no cargo administrador!", player.id);
    }
    
    return false;
  } else if (["!removeadmin"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      if (command[1].length > 0 && !Number.isNaN(Number.parseInt(command[1][1]))) {
        var lista = room.getPlayerList().filter((player) => player.id == Number.parseInt(command[1][1]));
  			lista.forEach(function(admin) {
          if (admin.name == "Rattatattmann") {
            room.sendAnnouncement(player.name + ' acha que pode remover Rattatattmann do cargo de administrador', undefined, undefined, "bold");
            room.sendAnnouncement('Olha que burrão!!!!!!', undefined, undefined, "bold");
          } else {
            room.setPlayerAdmin(Number.parseInt(command[1][1]), false);
            adminPassword = 100 + getRandomInt(900);
            room.sendAnnouncement('Agora você não é mais um administrador', Number.parseInt(command[1][1]), undefined, "bold");
            room.sendAnnouncement('Cargo removido por ' + player.name, Number.parseInt(command[1][1]), undefined, "bold");
          }
        });
      }
    } else {
      room.sendAnnouncement("Apenas um administrador pode remover algum jogador do cargo de administrador!", player.id);
    }
    
    return false;
  } else if (["!claim"].includes(command[0].toLowerCase())) {
    if (command[1].trim() == adminPassword) {
      room.setPlayerAdmin(player.id, true);
      adminPassword = 100 + getRandomInt(900);
    }
    
    return false;
  } else if (["!senha"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      room.setPassword(command[1].trim());
      roomPassword = command[1].trim();
      room.sendAnnouncement("Senha alterada por " + player.name);
    } else {
      room.sendAnnouncement("Apenas um administrador pode mudar a senha!", player.id);
    }
    
    return false;
  } else if (["!limparsenha"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      room.setPassword(null);
      roomPassword = null;
      room.sendAnnouncement("Senha removida por " + player.name);
    } else {
      room.sendAnnouncement("Apenas um administrador pode remover a senha!", player.id);
    }
    
    return false;
  } else if (["!rr"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      room.stopGame();
      room.startGame();
      room.sendAnnouncement("Partida reiniciada!", undefined, undefined, "bold");
    } else {
      room.sendAnnouncement("Apenas um administrador pode reiniciar o jogo!", player.id);
    }
    
    return false;
  } else if (["!kick"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      if (command[1].length > 0 && !Number.isNaN(Number.parseInt(command[1][1]))) {
        var lista = room.getPlayerList().filter((player) => player.id == Number.parseInt(command[1][1]));
        lista.forEach(function(kickado) {
          room.kickPlayer(kickado.id, message.substring(9).trim(), false);
          room.sendAnnouncement(kickado.name + " foi kickado por " + player.name);
        });
      }
    } else {
      room.sendAnnouncement("Apenas um administrador pode kickar jogadores!", player.id);
    }
    
    return false;
  } else if (["!ban"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      if (command[1].length > 0 && !Number.isNaN(Number.parseInt(command[1][1]))) {
        var lista = room.getPlayerList().filter((player) => player.id == Number.parseInt(command[1][1]));
        lista.forEach(function(banido) {
          if (banido.admin) {
            room.setPlayerAdmin(banido.id, false);
          }
          
          room.kickPlayer(banido.id, message.substring(8).trim(), true);
          room.sendAnnouncement('Ahhhhh ta banido!!!!!');
          room.sendAnnouncement(banido.name + " foi banido por " + player.name);
        });
      }
    } else {
      room.sendAnnouncement("Apenas um administrador pode banir jogadores!", player.id);
    }
    
    return false;
  } else if (["!iniciar"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      room.startGame();
      room.sendAnnouncement("Partida iniciada por " + player.name + "!", undefined, undefined, "bold");
    } else {
      room.sendAnnouncement("Apenas um administrador pode iniciar a partida!", player.id);
    }
  } else if (["!parar"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      room.stopGame();
      room.sendAnnouncement("Partida encerrada por " + player.name + "!", undefined, undefined, "bold");
    } else {
      room.sendAnnouncement("Apenas um administrador pode encerrar a partida!", player.id);
    }
  } else if (["!pause"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      if (!paused) {
        paused = true;
        room.pauseGame(true);
        room.sendAnnouncement("Partida pausada por " + player.name + "!", undefined, undefined, "bold");
        pauseAdmin = true;
      }
    } else {
      room.sendAnnouncement("Apenas um administrador pode pausar a partida!", player.id);
    }
    
    return false;
  } else if (["!unpause"].includes(command[0].toLowerCase())) {
    if (player.admin) {
      if (paused) {
        paused = false;
        room.pauseGame(false);
        room.sendAnnouncement("Partida retomada por " + player.name + "!", undefined, undefined, "bold");
        pauseAdmin = false;
      }
    } else {
      room.sendAnnouncement("Apenas um administrador pode retomar a partida!", player.id);
    }
    
    return false;
  }
  
  if (command[0][0] == "!") {
    return false;
  }
  
  if (player.admin) {
    room.sendAnnouncement('[Admin] ' + player.name + ": " + message.trim(), undefined, AdminColor, "bold");
    
    return false;
  } else {
    room.sendAnnouncement(player.name + ": " + message.trim());
    
    return false;
  }
}

room.onPlayerActivity = function(player) {
}

room.onPlayerBallKick = function(player) {
  if ((room.getBallPosition().x > 0 && room.getBallPosition().x < 10) || (room.getBallPosition().x < 0 && room.getBallPosition().x > -10)) {
    var blockedBall = true;
  } else {
    var blockedBall = false;
  }
  
  if ((teamR.length > 1) && (teamB.length > 1)) {
    if (lastPlayersTouched[3] != null) {
      if (lastPlayersTouched[3].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    } else if (lastPlayersTouched[2] != null) {
      if (lastPlayersTouched[2].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    } else if (lastPlayersTouched[1] != null) {
      if (lastPlayersTouched[1].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    } else if (lastPlayersTouched[0] != null) {
      if (lastPlayersTouched[0].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    }
    
    if (lastPlayersTouched[0] == null) {
        lastPlayersTouched[0] = player;
        console.log('1 toque do time ' + (lastPlayersTouched[0].team === 1 ? 'vermelho' : 'azul'));
    } else if (lastPlayersTouched[1] == null) {
      if (player.id != lastPlayersTouched[0].id) {
        lastPlayersTouched[1] = player;
        console.log('2 toques do time ' + (lastPlayersTouched[1].team === 1 ? 'vermelho' : 'azul'));
      } else {
        if (lastPlayersTouched[0].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[0].name + '!', undefined, undefined, "bold");
              console.log('2 toques do mesmo jogador do time vermelho');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[0].name + '!', undefined, undefined, "bold");
            console.log('2 toques do mesmo jogador do time vermelho');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[0].name + '!', undefined, undefined, "bold");
              console.log('2 toques do mesmo jogador do time azul');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[0].name + '!', undefined, undefined, "bold");
            console.log('2 toques do mesmo jogador do time azul');
          }
        }
      }
    } else if (lastPlayersTouched[2] == null) {
      if (player.id != lastPlayersTouched[1].id) {
        lastPlayersTouched[2] = player;
        console.log('3 toques do time ' + (lastPlayersTouched[2].team === 1 ? 'vermelho' : 'azul'));
      } else {
        if (lastPlayersTouched[0].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[1].name + '!', undefined, undefined, "bold");
              console.log('3 toques do mesmo jogador do time vermelho');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[1].name + '!', undefined, undefined, "bold");
            console.log('3 toques do mesmo jogador do time vermelho');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[1].name + '!', undefined, undefined, "bold");
              console.log('3 toques do mesmo jogador do time azul');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[1].name + '!', undefined, undefined, "bold");
            console.log('3 toques do mesmo jogador do time azul');
          }
        }
      }
    } else if (blockedBall === true && lastPlayersTouched[3] == null) {
      if (player.id != lastPlayersTouched[2].id) {
        lastPlayersTouched[3] = player;
        console.log('4 toques do time ' + (lastPlayersTouched[3].team === 1 ? 'vermelho' : 'azul'));
      } else {
        if (lastPlayersTouched[0].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[2].name + '!', undefined, undefined, "bold");
              console.log('4 toques do mesmo jogador do time vermelho');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[2].name + '!', undefined, undefined, "bold");
            console.log('4 toques do mesmo jogador do time vermelho');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[2].name + '!', undefined, undefined, "bold");
              console.log('4 toques do mesmo jogador do time azul');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Deixa seus colegas jogarem também, ' + lastPlayersTouched[2].name + '!', undefined, undefined, "bold");
            console.log('4 toques do mesmo jogador do time azul');
          }
        }
      }
    } else {
      if (lastPlayersTouched[3] == null) {
        if (lastPlayersTouched[2].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Time vermelho deu mais de 3 toques na bola!', undefined, undefined, "bold");
              console.log('Mais de 3 toques do time sem bloqueio');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Time vermelho deu mais de 3 toques na bola!', undefined, undefined, "bold");
            console.log('Mais de 3 toques do time sem bloqueio');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Time azul deu mais de 3 toques na bola!', undefined, undefined, "bold");
              console.log('Mais de 3 toques do time sem bloqueio');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Time azul deu mais de 3 toques na bola!', undefined, undefined, "bold");
            console.log('Mais de 3 toques do time sem bloqueio');
          }
        }
      } else {
        if (lastPlayersTouched[3].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Time vermelho deu mais de 4 toques na bola!', undefined, undefined, "bold");
              console.log('Mais de 4 toques');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Time vermelho deu mais de 4 toques na bola!', undefined, undefined, "bold");
            console.log('Mais de 4 toques');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Time azul deu mais de 4 toques na bola!', undefined, undefined, "bold");
              console.log('Mais de 4 toques');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Time azul deu mais de 4 toques na bola!', undefined, undefined, "bold");
            console.log('Mais de 4 toques');
          }
        }
      }
    }
  } else {
    if (lastPlayersTouched[3] != null) {
      if (lastPlayersTouched[3].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    } else if (lastPlayersTouched[2] != null) {
      if (lastPlayersTouched[2].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    } else if (lastPlayersTouched[1] != null) {
      if (lastPlayersTouched[1].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    } else if (lastPlayersTouched[0] != null) {
      if (lastPlayersTouched[0].team !== player.team) {
        lastPlayersTouched = [null, null];
      }
    }
    
    if (lastPlayersTouched[0] == null) {
        lastPlayersTouched[0] = player;
        console.log('1 toque do ' + lastPlayersTouched[0].name);
    } else if (lastPlayersTouched[1] == null) {
      if (player.id == lastPlayersTouched[0].id) {
        lastPlayersTouched[1] = player;
        console.log('2 toques do ' + lastPlayersTouched[0].name);
      }
    } else if (lastPlayersTouched[2] == null) {
      if (player.id == lastPlayersTouched[1].id) {
        lastPlayersTouched[2] = player;
        console.log('3 toques do ' + lastPlayersTouched[0].name);
      }
    } else if (blockedBall === true && lastPlayersTouched[3] == null) {
      if (player.id != lastPlayersTouched[2].id) {
        lastPlayersTouched[3] = player;
        console.log('4 toques do ' + lastPlayersTouched[0].name);
      } else {
        if (lastPlayersTouched[0].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
              console.log('4 toques do mesmo jogador');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
            console.log('4 toques do mesmo jogador');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
              console.log('4 toques do mesmo jogador');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
            console.log('4 toques do mesmo jogador');
          }
        }
      }
    } else {  
      if (lastPlayersTouched[3] == null) {
        if (lastPlayersTouched[2].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
              console.log('Mais de 3 toques sem bloqueio');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
            console.log('Mais de 3 toques sem bloqueio');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
              console.log('Mais de 3 toques sem bloqueio');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
            console.log('Mais de 3 toques sem bloqueio');
          }
        }
      } else {
        if (lastPlayersTouched[3].team === 1) {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
              console.log('Mais de 4 toques');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
            console.log('Mais de 4 toques');
          }
        } else {
          if (room.getBallPosition().y >= player.position.y) {
            if (player.position.y < 50 && player.position.y > -50) {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
              room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
              console.log('Mais de 4 toques');
            } else {
              room.setDiscProperties(0, {x: room.getBallPosition().x, y: 0});
            }
          } else {
            room.setDiscProperties(0, {x: room.getBallPosition().x, y: 500});
            room.sendAnnouncement('Mais toques do que permitido', undefined, undefined, "bold");
            console.log('Mais de 4 toques');
          }
        }
      }
    }
  }
}

/* GAME MANAGEMENT */

room.onGameStart = function(byPlayer) {
  goldenGoal = false;
  lastPlayersTouched = [null, null];
}

room.onGameStop = function(byPlayer) {
}

room.onGamePause = function(byPlayer) {
}

room.onGameUnpause = function(byPlayer) {
}

room.onTeamGoal = function(team) {
  const scores = room.getScores();
  
  for (var i = 3; i >= 0; i--) {
    if ((lastPlayersTouched[i] != null && lastPlayersTouched[i].team == team)) {
      room.sendAnnouncement("Tempo: " + getTime(scores), undefined, (team == Team.RED ? TeamRedColor : TeamBlueColor), "bold");
      room.sendAnnouncement("Ponto do time " + (team == Team.RED ? "vermelho" : "azul"), undefined, (team == Team.RED ? TeamRedColor : TeamBlueColor), "bold");
      room.sendAnnouncement("Corte de " + (lastPlayersTouched[i].team == team ? lastPlayersTouched[i].name : lastPlayersTouched[i].name) + "!", undefined, (team == Team.RED ? TeamRedColor : TeamBlueColor), "bold");
      room.sendAnnouncement("Velocidade do corte: " + ballSpeed.toPrecision(4).toString() + " km/h", undefined, (team == Team.RED ? TeamRedColor : TeamBlueColor), "bold");
      
      break;
    }
  }
  
  if (pauseNext) {
    pausaTecnica();
  }

  if (scores.red == scores.scoreLimit || scores.blue == scores.scoreLimit || goldenGoal == true) {
    endGame(team);
    goldenGoal = false;
    setTimeout(() => { room.stopGame(); }, 1000);
  }
}

room.onPositionsReset = function() {
  lastPlayersTouched = [null, null];
}

/* MISCELLANEOUS */

room.onRoomLink = function(url) {
}

room.onPlayerAdminChange = function(changedPlayer, byPlayer) {
}

room.onStadiumChange = function(newStadiumName, byPlayer) {
}

room.onGameTick = function() {
  checkTime();
  getStats();
}
